import { render } from 'react-dom'
import React from 'react'
import Calc from './Calc'

render((
  <Calc />
), document.getElementById('calc'));
