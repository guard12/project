import ThemedStyleSheet from 'react-with-styles/lib/ThemedStyleSheet';
import DefaultTheme from 'react-dates/lib/theme/DefaultTheme';

ThemedStyleSheet.registerTheme(DefaultTheme);

import 'react-dates/initialize'
import React from 'react'
import NumericInput from 'react-numeric-input'
import DatePicker from './DatePicker'

class Calc extends React.Component{
  constructor(props){
    super(props);

    this.handleDaysChange = this.handleDaysChange.bind(this);
    this.handleStartHourChange = this.handleStartHourChange.bind(this);
    this.handleEndHourChange = this.handleEndHourChange.bind(this);
    this.handleRateChange = this.handleRateChange.bind(this);
    this.handleBalanceChange = this.handleBalanceChange.bind(this);

    this.state = {
      Days: '',
      startHour: '',
      endHour: '',
      Rate: '',
      Balance: '',
      currentBalance: ''
    };

  }

  handleDaysChange(event){
    event.preventDefault();
    this.setState({
      Days: parseInt(event.target.value)
    })
  }

  handleStartHourChange(event){
    event.preventDefault();
    this.setState({
      startHour: parseInt(event.target.value)
    })
  }

  handleEndHourChange(event){
    event.preventDefault();
    this.setState({
      endHour: parseInt(event.target.value)
    })
  }

  handleRateChange(){
    this.setState({
      Rate: parseFloat(event.target.value)
    })
  }

  handleBalanceChange(event){
    event.preventDefault();
    this.setState({
      Balance: parseInt(event.target.value)
    })
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.balance(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  balance(){
    this.setState({
      currentBalance: this.state.Balance + (this.state.Rate * this.state.Hours) * this.state.Days
    })
  }

render(){
  return(
    <div>
    
    <label>Days: </label>
    <input type="number" value={this.state.Days} onChange={this.handleDaysChange} placeholder="Days" />
    <label>Start Hour: </label>
    <input type="number" value={this.state.startHour} onChange={this.handleStartHourChange} placeholder="Hours" />
    <label>End Hour: </label>
    <input type="number" value={this.state.endHour} onChange={this.handleEndHourChange} placeholder="Hours" />
    <label>Rate: </label>
    <NumericInput step={0.01} precision={2} value={this.state.Rate} onChange={this.handleRateChange} placeholder="Rate" />
    <label>Final balance: </label>
    <input type="number" value={(this.state.Rate * this.state.Hours) * this.state.Days } placeholder="Total" />
    <br />
    <input type="number" value={this.state.Balance} onChange={this.handleBalanceChange} placeholder="Balance" />
    <label>Current balance: </label>
    <input type="number" value={this.state.currentBalance} />
    </div>
  );
}

}
export default Calc;
